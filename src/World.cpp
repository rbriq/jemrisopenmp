/** @file World.cpp
 *  @brief Implementation of JEMRIS World
 */

/*
 *  JEMRIS Copyright (C) 
 *                        2006-2015  Tony Stoecker
 *                        2007-2015  Kaveh Vahedipour
 *                        2009-2015  Daniel Pflugfelder
 *                                  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "World.h"
#include "Model.h"
#include "SequenceTree.h"


/***********************************************************/
 World::FPTR World::saveEvolFunPtr;
 long World::TotalADCNumber;
 long World:: TotalSpinNumber;
 double World:: PhaseLock;
 double World:: RandNoise;
 double World:: GMAXoverB0;
 SequenceTree* World:: pSeqTree;
 AtomicSequence* World:: pStaticAtom;
 int World:: saveEvolStepSize;
 string World:: saveEvolFileName;
 ofstream* World:: saveEvolOfstream;
 int World:: m_myRank;
 bool World:: m_useLoadBalancing;
 int World:: m_no_processes;
 long World:: m_startSpin;
 int World:: m_noofspinprops;
 int World:: m_noofspincompartments;
 std::vector<double> World:: helper;
 multimap<EddyPulse*,double> World:: m_eddies;


World::World() {
		 time                =  0.0;
		 total_time          =  0.0;
		 phase               = -1.0;
		 deltaB              =  0.0;
		 GMAXoverB0          =  0.0;
		 NonLinGradField     =  0.0;
		 RandNoise           =  0.0;
		 saveEvolStepSize    =  0;
		 saveEvolFileName    =  "";
		 saveEvolOfstream    = NULL;
		 saveEvolFunPtr      = &Model::saveEvolution;
		 solverSuccess       = true;
		 m_noofspinprops     = 9;
		 solverSettings      = NULL;
		 pSeqTree            = NULL;
		 pAtom               = NULL;
		 pStaticAtom         = NULL;

		 m_myRank            = -1;
		 m_useLoadBalancing  = true;
		 m_no_processes      = 1;  /* default: serial jemris */
		 m_startSpin         = 0;
		 XMLPlatformUtils::Initialize ();
		 setValuesBySpinProps();
}

	World::World(const World& sourceWorld) {
		auxiliary = sourceWorld.auxiliary;
		 TotalADCNumber = sourceWorld.TotalADCNumber;
		 TotalSpinNumber = sourceWorld.TotalSpinNumber;
		 time = sourceWorld.time;
		 total_time = sourceWorld.total_time;
		 phase = sourceWorld.phase;
		 deltaB = sourceWorld.deltaB;
		 GMAXoverB0 = sourceWorld.GMAXoverB0;
		 NonLinGradField     =  sourceWorld.NonLinGradField;
		 RandNoise           =  sourceWorld.RandNoise;
		 saveEvolStepSize    =  sourceWorld.saveEvolStepSize;
		 saveEvolFileName    =  sourceWorld.saveEvolFileName;
		 saveEvolOfstream    = sourceWorld.saveEvolOfstream;
		 saveEvolFunPtr      = sourceWorld.saveEvolFunPtr;
		 solverSuccess       = sourceWorld.solverSuccess;
		 m_noofspinprops     = sourceWorld.m_noofspinprops;
		 solverSettings      = sourceWorld.solverSettings;
		 pSeqTree            = sourceWorld.pSeqTree;
		 pAtom               = sourceWorld.pAtom;
		 pStaticAtom         = sourceWorld.pStaticAtom;

		 m_myRank            = sourceWorld.m_myRank;
		 m_useLoadBalancing  = sourceWorld.m_useLoadBalancing;
		 m_no_processes      = sourceWorld.m_no_processes;  /* default: serial jemris */
		 m_startSpin         = sourceWorld.m_startSpin;


}
/***********************************************************/
double World::ConcomitantField (double* G) {

	if (GMAXoverB0==0.0) 
		return 0.0;

	return ((0.5*GMAXoverB0)*(pow(G[0]*Values[ZC]-0.5*G[2]*Values[XC],2) + pow(G[1]*Values[ZC]-0.5*G[2]*Values[YC],2))) ;

}

/***********************************************************/
 void World::SetNoOfSpinProps (int n) {

	// valid also for multi pool sample
	if ( m_noofspincompartments > 1 ){
		int m_ncoprops =  (n - 4) / m_noofspincompartments;
		m_noofspinprops = n;

	}else{
		m_noofspinprops = n;
	}
}

void World::setValuesBySpinProps(){
	if ( m_noofspincompartments > 1 ){
			int m_ncoprops =  (m_noofspinprops - 4) / m_noofspincompartments;
			Values = new double [m_noofspinprops*m_ncoprops];
			for ( int i = 0; i < m_noofspinprops; i++ )
				for ( int j = 0; j<m_ncoprops; j++ )
					Values [m_ncoprops*j+i] = 0.0;
		}else{
			Values = new double [m_noofspinprops];
			for ( int i = 0; i < m_noofspinprops; i++ )
				Values [i] = 0.0;
		}

    if (solution.empty())
        solution.resize(m_noofspincompartments * 3);
}

 void World::InitHelper (long size)  {
  if (size > 0)
    helper.resize(size);

}

 int World::GetNoOfCompartments () {
	return m_noofspincompartments;
}

 void World::SetNoOfCompartments (int n) {

	m_noofspincompartments = n;

}
   

World::~World () { 
	
	if (Values)
		delete Values; 
	
}
